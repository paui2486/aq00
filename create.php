<?php
    session_start();
    if($_SESSION['name'] != null)
    {

    }
    else
    {
        echo("權限不足，動導向致使首頁");
        echo("<meta http-equiv=REFRESH CONTENT=2;url=login.php>");
    }
?>

<?php
    
    require 'PDOCON.php';
    $name = $_SESSION['name']; 
    if ( !empty($_POST)) {
        // keep track validation errors
        
        $engError = null;
        $chiError = null;
        $chicnError = null;
        $chihkError = null;
        $chisigError = null;
        $malError = null;
        // keep track post values

        $eng = $_POST['eng'];
        $chi = $_POST['chi'];
        $chicn = $_POST['chicn'];
        $chihk = $_POST['chihk'];
        $chisig = $_POST['chisig'];
        $mal = $_POST['mal'];
        
         
        // validate input
        $valid = true;
        
        if (empty($eng)) {
            $engError = 'Please enter 英文';
            $valid = false;
        }
         
        if (empty($chi)) {
            $chiError = 'Please enter 中文';
            $valid = false;
        }
        
        if (empty($chicn)) {
            $chicnError = 'Please enter 大陸中文';
            $valid = false;
        }
        
        if (empty($chihk)) {
            $chihkError = 'Please enter 香港中文';
            $valid = false;
        }
        
        if (empty($chisig)) {
            $chisigError = 'Please enter 新加坡中文';
            $valid = false;
        }
        
        if (empty($mal)) {
            $malError = 'Please enter 馬來文';
            $valid = false;
        }
          
        // insert data
        if ($valid) {
            $pdo = Database::connect();
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "INSERT INTO translation (name,eng,chi,chicn,chihk,chisig,mal) values(?, ?, ?, ?, ?, ?, ?)";//
            $q = $pdo->prepare($sql);
            $q->execute(array($name,$eng,$chi,$chicn,$chihk,$chisig,$mal));
            Database::disconnect();
            header("Location: home.php");
        }
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<meta http-equiv="Content-Type" content="text/html" charset = "utf-8">
</head>
 
<body>
    <div class="container">
     
                <div class="span10 offset1">
                    <div class="row">
                        <h3>輸入翻譯對照</h3>
                    </div>
             
                    <form class="form-horizontal" action="create.php" method="post">

                      <div class="control-group <?php echo !empty($engError)?'error':'';?>">
                        <label class="control-label">英文原文</label>
                        <div class="controls">
                            <input name="eng" type="text" placeholder="輸入英文" style="width:400px;height:120px;" value="<?php echo !empty($eng)?$eng:'';?>">
                            <?php if (!empty($engError)): ?>
                                <span class="help-inline"><?php echo $engError;?></span>
                            <?php endif; ?>
                        </div>
                      </div>
                      <div class="control-group <?php echo !empty($chiError)?'error':'';?>">
                        <label class="control-label">台灣中文翻譯</label>
                        <div class="controls">
                            <input name="chi" type="text" placeholder="輸入台灣中文" style="width:400px;height:120px;" value="<?php echo !empty($chi)?$chi:'';?>">
                            <?php if (!empty($chiError)): ?>
                                <span class="help-inline"><?php echo $chiError;?></span>
                            <?php endif; ?>
                        </div>
                      </div>
                      <div class="control-group <?php echo !empty($chicnError)?'error':'';?>">
                        <label class="control-label">大陸中文翻譯</label>
                        <div class="controls">
                            <input name="chicn" type="text" placeholder="輸入大陸中文" style="width:400px;height:120px;" value="<?php echo !empty($chicn)?$chicn:'';?>">
                            <?php if (!empty($chicnError)): ?>
                                <span class="help-inline"><?php echo $chicnError;?></span>
                            <?php endif; ?>
                        </div>
                      </div>
                      <div class="control-group <?php echo !empty($chihkError)?'error':'';?>">
                        <label class="control-label">香港中文翻譯</label>
                        <div class="controls">
                            <input name="chihk" type="text" placeholder="輸入香港中文" style="width:400px;height:120px;" value="<?php echo !empty($chihk)?$chihk:'';?>">
                            <?php if (!empty($chihkError)): ?>
                                <span class="help-inline"><?php echo $chihkError;?></span>
                            <?php endif; ?>
                        </div>
                      </div>
                      <div class="control-group <?php echo !empty($chisigError)?'error':'';?>">
                        <label class="control-label">新加坡中文翻譯</label>
                        <div class="controls">
                            <input name="chisig" type="text" placeholder="輸入新加坡中文" style="width:400px;height:120px;" value="<?php echo !empty($chisig)?$chisig:'';?>">
                            <?php if (!empty($chisigError)): ?>
                                <span class="help-inline"><?php echo $chisigError;?></span>
                            <?php endif; ?>
                        </div>
                      </div>
                      <div class="control-group <?php echo !empty($malError)?'error':'';?>">
                        <label class="control-label">馬來文翻譯</label>
                        <div class="controls">
                            <input name="mal" type="text" placeholder="輸入馬來文" style="width:400px;height:120px;" value="<?php echo !empty($mal)?$mal:'';?>">
                            <?php if (!empty($malError)): ?>
                                <span class="help-inline"><?php echo $malError;?></span>
                            <?php endif; ?>
                        </div>
                      </div>
                      <div class="form-actions">
                          <button type="submit" class="btn btn-success">送出</button>
                          <a class="btn btn-danger" href="home.php">返回</a>
                        </div>
                    </form>
                </div>
                 
    </div> <!-- /container -->
  </body>
</html>


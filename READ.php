<?php
    require 'PDOCON.php';
    $ID = null;
    if ( !empty($_GET['ID'])) { //如果$_GET['ID'] 不為空值 則用$_REQUEST 接????
        //$ID = $_REQUEST['ID'];
        $ID = $_GET['ID'];
    }
     
    if ( null==$ID ) {
        header("Location: pdoindex.php");
    } else {
        $pdo = Database::connect();
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = "SELECT ID,eng,chi,name,chicn,chisig,chihk,mal FROM translation where ID = ?";
        $q = $pdo->prepare($sql);
        $q->execute(array($ID));
        $data = $q->fetch(PDO::FETCH_ASSOC);
        Database::disconnect();
    }
?>
 
<!DOCTYPE html>
<html lang="en">
<head>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<meta charset="utf-8">

</head>
 
<body>
    <div class="container">
     
                <div class="span10 offset1">
                    <div class="row">
                        <h3>READ</h3>
                    </div>
                     
                    <div class="form-horizontal" >
                      <div class="panel panel-primary">
                        <label class="control-label"> ID</label>
                        <div class="controls">
                            <label class="checkbox">
                                <?php echo '&nbsp;&nbsp;'.$data['ID'].'<br><br>';?>
                            </label>
                        </div>
                      </div>
                      <div class="panel panel-primary">
                        <label class="control-label"> NAME</label>
                        <div class="controls">
                            <label class="checkbox">
                                <?php echo '&nbsp;&nbsp;'.$data['name'].'<br><br>';?>
                            </label>
                        </div>
                      </div>
                      <div class="panel panel-primary">
                        <label class="control-label"> ENG</label>
                        <div class="controls">
                            <label class="checkbox">
                                <?php echo '<h3>&nbsp;&nbsp;'. $data['eng'].'</h3><br><br>';?>
                            </label>
                        </div>
                      </div>
                      <div class="panel panel-primary">
                        <label class="control-label"> CHI</label>
                        <div class="controls">
                            <label class="checkbox">
                                <?php echo '<h3>&nbsp;&nbsp;'.$data['chi'].'</h3><br><br>';?>
                            </label>
                        </div>
                      </div>
                      <div class="panel panel-primary">
                        <label class="control-label"> CHICN</label>
                        <div class="controls">
                            <label class="checkbox">
                                <?php echo '<h3>&nbsp;&nbsp;'.$data['chicn'].'</h3><br><br>';?>
                            </label>
                        </div>
                      </div>
                      <div class="panel panel-primary">
                        <label class="control-label"> CHIHK</label>
                        <div class="controls">
                            <label class="checkbox">
                                <?php echo '<h3>&nbsp;&nbsp;'.$data['chihk'].'</h3><br><br>';?>
                            </label>
                        </div>
                      </div>
                      <div class="panel panel-primary">
                        <label class="control-label"> CHISIG</label>
                        <div class="controls">
                            <label class="checkbox">
                                <?php echo '<h3>&nbsp;&nbsp;'.$data['chisig'].'</h3><br><br>';?>
                            </label>
                        </div>
                      </div>
                      <div class="panel panel-primary">
                        <label class="control-label"> Malay</label>
                        <div class="controls">
                            <label class="checkbox">
                                <?php echo '<h3>&nbsp;&nbsp;'.$data['mal'].'</h3><br><br>';?>
                            </label>
                        </div>
                      </div>
                        <div class="form-actions">
                          <a class="btn btn-danger" href="home.php">Back</a>
                       </div>
                     
                      
                    </div>
                </div>
                 
    </div> <!-- /container -->
  </body>
</html>
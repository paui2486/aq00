<?php
    session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<meta http-equiv="Content-Type" content="text/html" charset = "utf-8">
</head>
 
<body>
    <div class="container">
     
                <div class="span10 offset1">
                    <div class="row">
                        <h3>測試帳號密碼加密</h3>
                    </div>
             
                    <form class="form-horizontal" action="MD5U.php" method="post">

                      <div class="control-group <?php echo !empty($nameError)?'error':'';?>">
                        <label class="control-label">帳號</label>
                        <div class="controls">
                            <input name="name" type="text" placeholder="輸入帳號" style="width:400px;height:120px;" value="<?php echo !empty($name)?$name:'';?>">
                            <?php if (!empty($nameError)): ?>
                                <span class="help-inline"><?php echo $nameError;?></span>
                            <?php endif;?>
                        </div>
                      </div>
                      <div class="control-group <?php echo !empty($pwdError)?'error':'';?>">
                        <label class="control-label">密碼</label>
                        <div class="controls">
                            <input name="pwd" type="text" placeholder="輸入密碼" style="width:400px;height:120px;" value="<?php echo !empty($pwd)?$pwd:'';?>">
                            <?php if (!empty($mobileError)): ?>
                                <span class="help-inline"><?php echo $pwdError;?></span>
                            <?php endif;?>
                        </div>
                      </div>
                      <div class="form-actions">
                          <button type="submit" class="btn btn-success">Create</button>
                          <a class="btn btn-danger" href="home.php">Back</a>
                        </div>
                    </form>
                </div>
                 
    </div> <!-- /container -->
  </body>
</html>

<?php
    session_start(); 
    require 'PDOCON.php';
    
    if ( !empty($_POST)) {
        // keep track validation errors
        //$IDError = null;
        $nameError = null;
        $pwdError = null;
         
        // keep track post values
        //$ID  = $_POST['ID'];
        $name = $_POST['name'];
        $pwd =  $_POST['pwd'];
         //echo $name;
         //echo $pwd;

        
          
        // insert data
        if ($name && $pwd != null ) {
            $pdo = Database::connect();
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "INSERT INTO MD5U (name,pwd) values(?, ?)";//
            $q = $pdo->prepare($sql);
            $q->execute(array($name,MD5($pwd)));
            Database::disconnect();
            //header("Location: home.php");
        }
    }
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<?php
 
//引入一個檔案即可
require_once('/home/ubuntu/workspace/workspace/PHPExcel/Classes/PHPExcel.php');// 輸出用
require_once('/home/ubuntu/workspace/workspace/PHPExcel/Classes/PHPExcel/Writer/Excel2007.php');// 輸出用
require_once('/home/ubuntu/workspace/workspace/PHPExcel/Classes/PHPExcel/IOFactory.php');//讀取用

$objPHPExcel = new PHPExcel(); //實作一個 PHPExcel
$objPHPExcel->getProperties()->setCreator("PHP") //建立者
        ->setLastModifiedBy("PHP")//上次修改
        ->setTitle("Title標題") //標題
        ->setSubject("Subject副標題")//副標題
        ->setDescription("Description說明")//說明
        ->setKeywords("Keywords關鍵字")//關鍵字
        ->setCategory("Category分類");//分類
        
//設定操作中的工作表
$objPHPExcel->setActiveSheetIndex(0); //指定目前要編輯的工作表 ，預設0是指第一個工作表
$sheetX = $objPHPExcel->getActiveSheet();

//將工作表命名
$sheetX->setTitle('第一張表');//第一個工作表 名稱
$sheetX->getColumnDimension('A')->setWidth(50); //設定欄寬
$sheetX->getColumnDimension('B')->setWidth(50);
$sheetX->getColumnDimension('C')->setWidth(50);
$sheetX->getColumnDimension('D')->setWidth(50);
//// 這以上是存檔需要的宣告 以下是讀檔必備的資料


//檔案路徑
$inputFileName = $newxlsx;
$inputFileType = PHPExcel_IOFactory::identify($inputFileName);// 讓程式自動判別副檔名(excel版本)
$reader = PHPExcel_IOFactory::createReader($inputFileType); // 讀取2007 excel 檔案
$PHPExcel = $reader->load($inputFileName); // 檔案名稱 需已經上傳到主機上
$sheet = $PHPExcel->getSheet(0); // 讀取第一個工作表(編號從 0 開始)

$inputFileName1 = $newxlsx;
$inputFileType1 = PHPExcel_IOFactory::identify($inputFileName1);// 讓程式自動判別副檔名(excel版本)
$reader1 = PHPExcel_IOFactory::createReader($inputFileType1); // 讀取2007 excel 檔案
$PHPExcel1 = $reader1->load($inputFileName1); // 檔案名稱 需已經上傳到主機上
$sheet1 = $PHPExcel1->getSheet(0); // 讀取第一個工作表(編號從 0 開始)

//$sheetCount = $PHPExcel->getSheetCount(); // 取得總工作表數 
$HighestColumn = $sheet->getHighestColumn(); //最大欄位的英文代號?
$highestRow = $sheet->getHighestRow(); // 取得總列數 數字編號。A=0, B=1, C=2

for($row=0;$row<=$highestRow;$row++){
$sheetX->getStyle('A'.$row)->getAlignment()->setWrapText(true);//單格設定 自動換列
$sheetX->getStyle('B'.$row)->getAlignment()->setWrapText(true);
}

$TotalColumn = (ord($HighestColumn)-(64));// 將"任意"字符轉換為 ASCII 碼: 這裡是將G轉換71-64=7 

//echo '總共 '.$HighestColumn;

echo '總共 '.$highestRow.' 列';
echo '總共 '.$TotalColumn.' 行';
// 一次讀取一列
//exit();
for ($row = 11; $row <= $highestRow; $row++) {
    
    $val = $sheet->getCellByColumnAndRow('0', $row)->getValue();//這裡是讀取 要用數字 0A 3D
    $val1 = $sheet->getCellByColumnAndRow('1', $row)->getValue();//B
    if($val1 == null){
    { //MODEtwo
    $i = $i+1;
    $X = $row -$i;
    $sheetX->setCellValue("A".($row-$X),$val);// 這裡是寫入 直接用英文
    }
    /* MODEone
    $sheetX->setCellValue("A".($row),$val);// 這裡是寫入 直接用英文
    */
    }else{
        
    }
}


// 存檔必須宣告的必要資訊

$filename = ($startX);

ob_end_clean();

$filename = $filename.'發翻譯'.'.xlsx';

header("Content-type: text/html; charset=utf-8");
header("Content-Type: application/vnd.ms-excel");
header("Content-Disposition: attachment;filename=".$filename);
header("Cache-Control: max-age=0");


$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
//$objWriter->save('filenameX.xlsx');// 另存成指定檔名
exit;// 注意 這個exit是必要的 雖然沒有程式 也會正常執行刑但是最後產生的excel會有問題

?>
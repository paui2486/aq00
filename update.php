<?php
    session_start();
    if($_SESSION['name'] != null)
    {

    }
    else
    {
        echo("權限不足，動導向致使首頁");
        echo("<meta http-equiv=REFRESH CONTENT=2;url=login.php>");
    }
?>


<?php
    require_once 'PDOCON.php';
    if ( !empty($_POST)) {
        // keep track validation errors
        $nameError = null;
        $engError = null;
        $chiError = null;
        $chicnError = null;
        $chihkError = null;
        $chisigError = null;
        $malError = null; 
        // keep track post values
        $ID = $_POST['ID'];
        $name  = $_POST['name'];
        $eng = $_POST['eng'];
        $chi = $_POST['chi'];
        $chicn = $_POST['chicn'];
        $chihk = $_POST['chihk'];
        $chisig = $_POST['chisig'];
        $mal = $_POST['mal'];
         
        // validate input
        $valid = true;
        /*
        if (empty($name)) {
            //$nameError = 'Please enter 名稱';
            //$valid = false;
        }
         
        if (empty($eng)) {
            $engError = 'Please enter 英文';
            $valid = false;
        }
         
        if (empty($chi)) {
            $chiError = 'Please enter 中文';
            $valid = false;
        }
        
        if (empty($chicn)) {
            $chicnError = 'Please enter 大陸中文';
            $valid = false;
        }
        
        if (empty($chihk)) {
            $chihkError = 'Please enter 香港中文';
            $valid = false;
        }
        
        if (empty($chisig)) {
            $chisigError = 'Please enter 新加坡中文';
            $valid = false;
        }
        
        if (empty($mal)) {
            $malError = 'Please enter 馬來文';
            $valid = false;
        }
        
        if (empty($ID)) {
            $IDError = 'Please enter ID';
            $valid = false;
        }
        */
        // update data
        if ($valid) {
            $pdo = Database::connect();
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "UPDATE translation set name = ?, eng = ?, chi =?, chicn =?, chihk =?, chisig =?, mal =? WHERE ID = ?";
            $q = $pdo->prepare($sql);
            $q->execute(array($name,$eng,$chi,$chicn,$chihk,$chisig,$mal,$ID));
            Database::disconnect();
            header("Location: home.php");
        }
    } else {
        $pdo = Database::connect();
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = "SELECT ID,eng,chi,chicn,chihk,chisig,mal,name FROM translation where ID = ?";
        $q = $pdo->prepare($sql);
        $q->execute(array($ID));
        $dota = $q->fetch(PDO::FETCH_ASSOC);
        $name = $dota['name'];
        $eng = $dota['eng'];
        $chi = $dota['chi'];
        $chicn = $dota['chicn'];
        $chihk = $dota['chihk'];
        $chisig = $dota['chisig'];
        $mal = $_POST['mal'];
        $ID = $_POST['ID']; 
        Database::disconnect();
    }
?>
<?php
        require_once 'PDOCON.php';
        $id = $_GET['id'];
        
        $pdo = Database::connect();
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = "SELECT ID,eng,chi,chicn,chihk,chisig,mal,name FROM translation where ID = ?";
        $q = $pdo->prepare($sql);
        $q->execute(array($id));
        $data = $q->fetch(PDO::FETCH_OBJ);
        Database::disconnect();
?>




<!DOCTYPE html>
<html lang="en">
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<meta http-equiv="Content-Type" content="text/html" charset = "utf-8">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script>
            function check_select(form){
            if(confirm("確定要送出此資料嗎？")){
                $("#id").removeAttr("disabled");
                $("#name").removeAttr("disabled"); //家在FORM onsubmit="return check_select()"
                return true;
            }else{
                return false;
            }
            }  
        </script>
</head>
 
<body>
    <div class="container">
     
                <div class="span10 offset1">
                    <div class="row">
                        <h3>輸入翻譯對照</h3>
                    </div>
             
                    <form class="form-horizontal" action="update.php" method="post" onsubmit="return check_select()">
                      <div class="control-group">
                        <label class="control-label">ID</label> 
                          <div class="controls">
                            <input id = "id" name="ID" type="text"  placeholder="輸入ID" disabled="disabled" value="<?php echo $id;?>">
                            
                        </div>
                        <label class="control-label">名稱</label>
                        <div class="controls">
                            <input id = "name" name="name" type="text"  placeholder="輸入名稱" disabled="disabled" value="<?php echo $data->name ;?>"><!-- disabled="disabled" 小心使用它會讓值傳不出來-->
                            
                        </div>
                      </div>
                      <div class="control-group <?php echo !empty($engError)?'error':'';?>">
                        <label class="control-label">英文原文</label>
                        <div class="controls">
                            <input id = "eng" name="eng" type="text" placeholder="輸入英文" style="width:400px;height:120px;"  value="<?php echo $data->eng ;?>">
                            
                                <span class="help-inline"><?php echo $engError;?></span>
                            
                        </div>
                      </div>
                      <div class="control-group <?php echo !empty($chiError)?'error':'';?>">
                        <label class="control-label">台灣中文翻譯</label>
                        <div class="controls">
                            <input id = "chi" name="chi" type="text"  placeholder="輸入中文" style="width:400px;height:120px;" value="<?php echo $data->chi ;?>">
                            <?php if (!empty($chiError)): ?>
                                <span class="help-inline"><?php echo $chiError;?></span>
                            <?php endif;?>
                        </div>
                      </div>
                      <div class="control-group <?php echo !empty($chicnError)?'error':'';?>">
                        <label class="control-label">大陸中文翻譯</label>
                        <div class="controls">
                            <input id = "chicn" name="chicn" type="text"  placeholder="輸入中文" style="width:400px;height:120px;" value="<?php echo $data->chicn ;?>">
                            <?php if (!empty($chicnError)): ?>
                                <span class="help-inline"><?php echo $chicnError;?></span>
                            <?php endif;?>
                        </div>
                      </div>
                      <div class="control-group <?php echo !empty($chihkError)?'error':'';?>">
                        <label class="control-label">香港中文翻譯</label>
                        <div class="controls">
                            <input name="chihk" type="text"  placeholder="輸入中文" style="width:400px;height:120px;" value="<?php echo $data->chihk ;?>">
                            
                                <span class="help-inline"><?php echo $chihkError;?></span>
                            
                        </div>
                      </div>
                      <div class="control-group <?php echo !empty($chisigError)?'error':'';?>">
                        <label class="control-label">新加坡中文翻譯</label>
                        <div class="controls">
                            <input name="chisig" type="text"  placeholder="輸入中文" style="width:400px;height:120px;" value="<?php echo $data->chisig ;?>">
                            
                                <span class="help-inline"><?php echo $chisigError;?></span>
                            
                        </div>
                      </div>
                      <div class="control-group <?php echo !empty($malError)?'error':'';?>">
                        <label class="control-label">馬來文翻譯</label>
                        <div class="controls">
                            <input name="mal" type="text"  placeholder="輸入中文" style="width:400px;height:120px;" value="<?php echo $data->mal ;?>">
                            
                                <span class="help-inline"><?php echo $malError;?></span>
                            
                        </div>
                      </div>
                      <div class="form-actions">
                          <button type="submit" class="btn btn-success">Update</button>
                          <a class="btn btn-danger" href="home.php">Back</a>
                        </div>
                    </form>
                </div>
                 
    </div> <!-- /container -->
  </body>
</html>


<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!DOCTYPE html>

<html>
<head>
    <!-- Latest compiled and minified CSS -->
    
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<meta charset="utf-8">

        <script>
            function check_select(form){
            if(confirm("確定要送出此資料嗎？")){
                $("#id").removeAttr("disabled");
                $("#tid").removeAttr("disabled");
                $("#eng").removeAttr("disabled");
                return true;
            }else{
                return false;
            }
            }  
        </script>

</head>
<?php
    session_start();
    if($_SESSION['name'] != null)
    {
    
    }
    else
    {
        echo("權限不足，動導向致使首頁");
        echo("<meta http-equiv=REFRESH CONTENT=1;url=login.php>");
    }
    $mem = new Memcached();
    $mem->addServer("127.0.0.1", 11211);
    $name = $_SESSION['name'];
    include("PDOCON.php");
    
    $id = null;
    $tid = null;
    if ( !empty($_GET['id'] or $_GET['tid'])) { //如果$_GET['ID'] 不為空值 PS $_REQUEST 可接 GET POST 但是不要常用會搗亂架構
        $id = $_GET['id'];
        $tid = $_GET['tid'];
    }
    
    if ( !empty($_POST)) {
        // keep track post values
        $id = $_POST['id'];
        $tid  = $_POST['tid'];
        $eng = $_POST['eng'];
        $chi = $_POST['chi'];
        /*
        $chicn = $_POST['chicn'];
        $chihk = $_POST['chihk'];
        $chisig = $_POST['chisig'];
        $mal = $_POST['mal'];
        */ 
        // validate input
        $valid = true;
        
        // update data
        if ($valid) {
            
            $pdo = Database::connect();
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "UPDATE ReviewB set eng = ?,chi = ? WHERE id = ? and tid = ?";
            $q = $pdo->prepare($sql);
            $q->execute(array($eng,$chi,$id,$tid));
            Database::disconnect();
            header("Location: Firsttrial.php");
        }
    } else {
        $pdo = Database::connect();
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = "SELECT tid,id,eng,chi FROM ReviewB where id = ? and tid = ?";
        $q = $pdo->prepare($sql);
        $q->execute(array($id,$tid));
        $dota = $q->fetch(PDO::FETCH_ASSOC);
        
        $eng = $dota['eng'];
        $chi = $dota['chi'];
        
        Database::disconnect();
    }
    
    ?>
    
    <?php
    //echo $id;
    //echo $tid;
        $pdo = Database::connect();
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = "SELECT * FROM ReviewB where id = ? and tid = ?";
        $q = $pdo->prepare($sql);
        $q->execute(array($id,$tid));
        $data = $q->fetch(PDO::FETCH_OBJ);
        Database::disconnect();
        
        //$eng = $dota['eng'];
        //$chi = $dota['chi'];
        
?>
    
<body>
    <div class="container">
     
                <div class="span10 offset1">
                    <div class="row">
                        <h3>輸入翻譯對照</h3>
                    </div>
             
                    <form class="form-horizontal" action="Reviewchange.php" method="post" onsubmit="return check_select()">
                      <div class="control-group">
                        <label class="control-label">ID</label> 
                          <div class="controls">
                            <input id = "id" name="id" type="text"  placeholder="輸入ID" disabled="disabled" value="<?php echo $id;?>">
                            
                        </div>
                        <label class="control-label">TID</label>
                        <div class="controls">
                            <input id = "tid" name="tid" type="text"  placeholder="輸入TID"  disabled="disabled" value="<?php echo $tid ;?>"><!-- disabled="disabled" 小心使用它會讓值傳不出來-->
                            
                        </div>
                      </div>
                      <div class="control-group">
                        <label class="control-label">英文原文</label>
                        <div class="controls">
                            <input id = "eng" name="eng" type="text" placeholder="輸入英文" style="width:400px;height:120px;" disabled="disabled" value="<?php echo $data->eng ;?>">
                                
                        </div>
                      </div>
                      <div class="control-group">
                        <label class="control-label">台灣中文翻譯</label>
                        <div class="controls">
                            <input id = "chi" name="chi" type="text"  placeholder="輸入中文" style="width:400px;height:120px;" value="<?php echo $data->chi ;?>">
                                
                        </div>
                      </div>
                      <div class="form-actions">
                          <button id = "submit" name="submit" type="submit" class="btn btn-success">Update</button>
                          <a class="btn btn-danger" href="Firsttrial.php">Back</a>
                       </div>
                      </form>
                </div>
                 
    </div> 
  </body>
</html>
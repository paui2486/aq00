<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<?php
require_once ('/var/www/html/AQ/PHPExcel/Classes/PHPExcel.php');
require_once ('/var/www/html/AQ/PHPExcel/Classes/PHPExcel/IOFactory.php');
require_once ("PDOCON.php");

$str = $_POST['eng'];
$FN = $_POST['filename'];

////function

function half($p1,$p2,$p3){
	if(preg_match("/$p2/i",$p1)){
				$key = "$p2";
			}elseif(preg_match("/$p3/i",$p1)){
				$key = "$p3";
			}
			
			$pdo = Database::connect();
			$sql = 'SELECT * FROM translation where eng LIKE ? ORDER BY timeatamp DESC LIMIT 1'; //  嘗試用LIKE如果直接比對 eng 的話 替換要比對的資料陣列為%
			$result=$pdo->prepare($sql);
			$result->execute(array($p1));// 將相容的資料塞入 LIKE 去搜尋
			Database::disconnect();
			$rw = $result->fetch(PDO::FETCH_OBJ);
			
			if($rw!=null){
				
				$valc = $p1;
				return $valc;
			}else{
				$valc = "%p.m%Welcome reception%$key%";
				return $valc;
				
			}
}

////

$str = str_replace("nbsp","+","$str");
$str = str_replace("⏎"," ","$str");
$str = str_replace("+","<br>","$str");
$str = str_replace("<br><br>","<br>","$str");
$str = str_replace("\r","","$str");
$str = preg_replace("/(CollapseURL: Package_.*_\{.*\}_.*json.*PM|AM$)/i","<br>","$str");//^開頭 .任意字元 *比對前一字元0個或是1以上 $結尾
$str = preg_replace("/(CollapseURL: Package_.*_\{.*\}_.*json.*AM|PM$)/i","<br>","$str");// p 莫名原因被吃掉了一組
$str = preg_replace("/(URL: Package_.*_\{.*\}_.*json.*PM|AM$)/i","<br>","$str");
$str = preg_replace("/(URL: Package_.*_\{.*\}_.*json.*AM|PM$)/i","<br>","$str");

/*
$value = 'imgurl=中"國&#12345;abc&#6754;d&ASBVF&12345;SSS[b]aa[/b]';
$value = preg_replace("/[\"]|(\&\#([0-9]{5})\;)|(\&\#([0-9]{4})\;)|(\&([0-9]{5})\;)|(\[b\])|(\[\/b\])|([imgurl=])/", '',$value);
echo $value;*/
//exit();
$c=array("");// 空集合
$cstr = strip_tags($str, "<br>");//整理過後的字串 過濾 程式標籤符號
$nstr = preg_split("/<br><br>|AssignToggle Dropdown|\r|\n|<br>/", $cstr);//wul6
$nstr = array_merge(array_filter($nstr),$c); ///刪除陣列中的空值

//var_dump($nstr);
//exit();

$NC = count($nstr); //小於等於使用

/*
$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$sql = "INSERT INTO wvlist (filename) values(?)";//
$q = $pdo->prepare($sql);
$q->execute(array($FN));
Database::disconnect();
*/

$objPHPExcel = new PHPExcel(); //實作一個 PHPExcel

//print_r($nstr);
//echo $FN;

//設定操作中的工作表
$objPHPExcel->setActiveSheetIndex(0); //指定目前要編輯的工作表 ，預設0是指第一個工作表
$sheet = $objPHPExcel->getActiveSheet();
 
//將工作表命名
$sheet->setTitle('第一張表');//第一個工作表 名稱

$sheet->getColumnDimension('A')->setWidth(50); //設定欄寬
$sheet->getColumnDimension('B')->setWidth(50);
$sheet->getColumnDimension('C')->setWidth(50);
$sheet->getColumnDimension('D')->setWidth(50);
$sheet->getColumnDimension('E')->setWidth(50);
//print_r ($nstr) ;

$sheet->setCellValue('A1','         　　 Aquaview Co. Ltd.');
$sheet->getStyle('A1')->getFont()->setBold(true);
$sheet->getStyle('A1')->getFont()->setSize(16);
$sheet->setCellValue('A2','　　         目川文化數位股份有限公司');
$sheet->getStyle('A2')->getFont()->setBold(true);
$sheet->getStyle('A2')->getFont()->setSize(11);
$sheet->setCellValue('A3','　　          Solution to Creative Learning');
$sheet->setCellValue('A5','URL');
$sheet->setCellValue('A6','各語言字數');
$sheet->setCellValue('A7','第幾篇');
$sheet->setCellValue("B"."9","TC (繁體版)");

$sheet->setCellValue('A10','文章名稱');
//接下來要寫一個 迴圈自動換列



for($i=1;$i<=$NC;$i++){

    $sheet->getStyle("A".($i+10))->getAlignment()->setWrapText(true);
	$sheet->getStyle("A".($i+10))->getFont()->setSize(11);
    $sheet->getStyle("A".($i+10))->getFont()->setName('微軟正黑體');
	$sheet->getStyle("A".($i+10))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);//靠左
	$sheet->getStyle("A".($i+10))->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);//垂直置頂
	
	$sheet->getStyle("B".($i+10))->getAlignment()->setWrapText(true);
	$sheet->getStyle("B".($i+10))->getFont()->setSize(11);
    $sheet->getStyle("B".($i+10))->getFont()->setName('微軟正黑體');
	$sheet->getStyle("B".($i+10))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);//靠左
	$sheet->getStyle("B".($i+10))->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);//垂直置頂
	
}

//儲存格原文內容
for($i=1;$i<=$NC;$i++){
    
    $sheet->setCellValue("A".($i+10),($nstr[($i-1)]));
    
}
// 邏輯上要先處理 進來的原文比對結果後直接存
for($i=1;$i<=$NC;$i++){//;$i<=$NC
	
		if(preg_match("/^\*URL../i", $nstr[($i-1)])){//如果VAL開頭是URL就處理
                $Newval = preg_split("/[\s]+/",trim($nstr[($i-1)]));
                $Newval[0] = null; //不應該 變成NULL 應該要把順序往前提
                $Newval[1] = null;
                array_splice($Newval,0,2);//
                $val = implode(" ",$Newval);
                $Newval = preg_split("/[\s]+/",$val);// 空白切割
                $Newval = preg_replace("/\d+/","%",$Newval);// 正規表達式 數字取代成%
                $valc = implode(" ",$Newval); //相容的
				$valc = trim("$valc");
                
                
        }else if(preg_match("/^Airfare is not included in the trip price\!.*We highly recommend that you purchase travel insurance for any trip.$/",$nstr[($i-1)])){
            //註解 頭尾比較關鍵字 .*可以 類是SQL%功能 .是任意字元 *: 出現 0 次或以上
            //$val = "Airfare is not included in the trip price! The closest commercial airport to the % is % International Airport % We highly recommend that you purchase travel insurance for any trip.";
            $Newval = preg_split("/[\s]+/",($nstr[($i-1)]));//根據空白切開 意思說 這邊根據單字做比對
            $Newval = preg_replace("/[\d]+/","%",$Newval);
            $valc = implode(" ",$Newval); //相容的
			$valc = trim("$valc");
        }else if(preg_match("/^Airfare is not included in the trip price\!.*Please note: Airport transfers are not included for.*.$/",$nstr[($i-1)])){
            
            //$val = "Airfare is not included in the trip price! The closest commercial airport to the hotel is %. Please note: Airport transfers are not included for %";
            $Newval = preg_split("/[\s]+/",($nstr[($i-1)]));
            $Newval = preg_replace("/[\d]+/","%",$Newval);
            $valc = implode(" ",$Newval); //相容的
			$valc = trim("$valc");
        }/*else if(preg_match("/^ $/i",$nstr[($i-1)])){
            
            $Newval = preg_split("/[\s]+/",($nstr[($i-1)]));//
            $valc = implode(" ",$Newval);
            $valc = trim("$valc");
            
        
        }*/else if(preg_match("/^Your DreamTrip includes.*days and.*as well as:$/i",$nstr[($i-1)])){
            
            $Newval = preg_split("/[\s]+/",($nstr[($i-1)]));//
			$d1 = $Newval[3];
			$d2 = $Newval[6];
			//echo "$d1"."$d2";
            $valc = "Your DreamTrip includes $d1 days and $d2%as well as:%";
			
			//echo $valc;
            //exit;
        }else if(preg_match("/.*a\.m\..*Full-day.*tour.*includes lunch and transportation/i",$nstr[($i-1)])){
            
            $Newval = preg_split("/[\s]+/",($nstr[($i-1)]));
			$d1 = $Newval[0];
			$valc = "$d1 a.m.%Full-day % tour;%includes lunch and transportation%";
            //echo $valc;
			//exit();
        }else if(preg_match("/.*p\.m\..*Welcome reception.*/i",$nstr[($i-1)])){ //// 進來的處理完在判斷 是旅館或是渡假村 測試進入後外加SQL查詢
            //7-8 p.m.: Welcome reception at the resort.
            $Newval = preg_split("/[\s]+/",($nstr[($i-1)]));
            $valc = trim(implode(" ",$Newval));
			$valc = half($valc,"htoel","resort");//第一格輸入欲比對內容,關鍵字一,關鍵字二
			//exit();
        }else if(preg_match("/^. p\.m\..*$|. a\.m\..*$/i",$nstr[($i-1)])){
            
            $Newval = preg_split("/[\s]+/",($nstr[($i-1)]));
            $valc = implode(" ",$Newval);
            $valc = trim("$valc");
            
        }else if(preg_match("/^TC無此段.*./i",$nstr[($i-1)])){
            //註解 頭尾比較關鍵字 .*可以 類是SQL%功能 .是任意字元 *: 出現 0 次或以上
            $Newval = preg_split("/[\s]+/",($nstr[($i-1)]));//根據空白切開 意思說 這邊根據單字做比對
            $valc = "— — —";
            
        }else if(preg_match("/^Your DreamTrip includes.*.days and.*.nights$/i",$nstr[($i-1)])){
            //註解 頭尾比較關鍵字 .*可以 類是SQL%功能 .是任意字元 *: 出現 0 次或以上
            $Newval = preg_split("/[\s]+/",($nstr[($i-1)]));//根據空白切開 意思說 這邊根據單字做比對
            $valc = implode(" ",$Newval);
            $valc = trim("$valc");
            
        }else if(preg_match("/^•	/i",$nstr[($i-1)])){//
            //註解 頭尾比較關鍵字 .*可以 類是SQL%功能 .是任意字元 *: 出現 0 次或以上
            $Newval = preg_split("/[\s]+/",($nstr[($i-1)]));
			$Newval[0] = null;
			$Newval = preg_replace("/\d+/","%",$Newval);
			
			$valc = (implode("%",$Newval));

            //echo $valc;
            //exit;
        }else if(preg_match("/^• /i",$nstr[($i-1)])){//
            //註解 頭尾比較關鍵字 .*可以 類是SQL%功能 .是任意字元 *: 出現 0 次或以上
            $Newval = preg_split("/[\s]+/",($nstr[($i-1)]));
			$Newval[0] = null;
			$Newval = preg_replace("/\d+/","%",$Newval);
			
			$valc = (implode("%",$Newval));

            //echo $valc;
            //exit;
        }else if(preg_match("/^DAY.[1-9].*$/i",$nstr[($i-1)])){
            
            $Newval = preg_split("/[\s]+/",($nstr[($i-1)]));//
            $valc = implode(" ",$Newval);
            $valc = trim("$valc");
            
        }else{
            $Newval = preg_split("/[\s]+/",$nstr[($i-1)]);//根據空白切開 意思說 這邊根據單字做比對

			$Newval = preg_replace("/[\d]+/","%",$Newval);
            //$valc = implode("%",$Newval); //相容的
			//print_r($Newval);
			
				//$last = (count($Newval))-1;// 最後一個元素
				//$Newval[$last] = "%";
				$valc = implode("%",$Newval); //相容的

				$valc = trim("$valc");
			//echo $valc;
			//exit();
				if($valc == "%"){$valc = '';}else{}
			}
	
		//echo $valc;
		//exit();
		// 比對開始
		$pdo = Database::connect();
        $sql = 'SELECT * FROM translation where eng LIKE ? ORDER BY timeatamp DESC LIMIT 1'; //嘗試用LIKE如果直接比對 eng 的話 替換要比對的資料陣列為%
        $result=$pdo->prepare($sql);
        $result->execute(array($valc));// 將相容的資料塞入 LIKE 去搜尋
        Database::disconnect();
        $rw = $result->fetch(PDO::FETCH_OBJ);
		//echo $sql;
		//echo $rw;
		//exit();
		
		if($rw != null){
			
               $translation = $rw->chi;    //跑到第幾個就叫出修對應的中文
               $chicn = $rw->chicn;
               $Melayu = $rw->mal;
               //$original = $nstr[($i-1)];
               
               //$sheet->setCellValue("A".($row),$original);
               
               $sheet->setCellValue("B".($i+10),$translation);
               
               //$sheet->setCellValue("C".($i+10),$chicn);// 簡體
               
               //$sheet->setCellValue("D".($i+10),$Melayu);// 馬來
               
        }else{
  
        }
		// 將內容貼上 結束
}


ob_end_clean();
$objPHPExcel = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

$filename = $FN.'.xlsx';
header("Content-type: text/html; charset=utf-8");
header("Content-Type: application/vnd.ms-excel");
header("Content-Disposition: attachment;filename=".$filename);
header("Cache-Control: max-age=0");


$objPHPExcel->save('php://output');
//$objWriter->save('filename.xlsx');// 另存成指定檔名
//exit;

?>
<?php
    session_start();
    if($_SESSION['name'] != null)
    {

    }
    else
    {
        echo("權限不足，動導向致使首頁");
        echo("<meta http-equiv=REFRESH CONTENT=2;url=login.php>");
    }
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!DOCTYPE html>
<html>
<head>
    <!-- Latest compiled and minified CSS -->
    
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<meta charset="utf-8">

</head>
 
<body>
    <div class="container">
            <div class="row">
                <h3>目川文化</h3>
            </div>
            <div class="row">
                <p>
                    
                    <a href="home.php" class="btn btn-success">返回</a>
                </p>
                
                <table class="table table-striped table-bordered">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>name</th>
                      <th>IP</th>
                      <th>時間</th>
                      
                    </tr>
                  </thead>
                  <tbody>
                  <?php
                   include 'PDOCON.php';
                   $pdo = Database::connect();
                   $sql = 'SELECT ID,name,IP,timestamp FROM userlogin  ORDER BY id DESC LIMIT 20'; //輸出10
                   
                   $result=$pdo->prepare($sql);
                   $result->execute();
                   
                   while($row = $result->fetch(PDO::FETCH_OBJ)){
                            echo '<tr>';
                            echo '<td>'. $row->ID."\n".'</td>';
                            echo '<td>'. $row->name."\n".'</td>';
                            echo '<td>'. $row->IP."\n".'</td>';
                            echo '<td>'. $row->timestamp."\n".'</td>';
                            echo '</tr>';
                   }
                   Database::disconnect();
                  ?>
                  </tbody>
            </table>
        </div>
    </div>
  </body>
</html>
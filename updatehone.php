<?php
    session_start();
    if($_SESSION['name'] != null)
    {

    }
    else
    {
        echo("權限不足，動導向致使首頁");
        echo("<meta http-equiv=REFRESH CONTENT=2;url=index.php>");
    }

    $name = $_SESSION['name'];
    include("PDOCON.php");
    if ( !empty($_POST)) {
        // keep track validation errors
        // keep track post values
        $id = $_POST['id'];
        $eng = $_POST['eng'];
        $chi = $_POST['chi'];
        $chicn = $_POST['chicn'];
        $chihk = $_POST['chihk'];
        $chisig = $_POST['chisig'];
        $mal = $_POST['mal'];
        
        // validate input
        $valid = true;

        // insert data
        if ($valid) {
            $pdo = Database::connect();
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "SELECT * FROM translation where ID = ? or eng LIKE ? or chi LIKE ?";
            $q = $pdo->prepare($sql);
            if ($chi!=null){$chi = "%"."$chi"."%";}
            if ($eng!=null){$eng = "%"."$eng"."%";}
            //exit();
            $q->execute(array($id,$eng,$chi));
            ?>
            
            <html>
                <body>
            
            <table class="table table-striped table-bordered">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>ENG</th>
                      <th>CHI</th>
                      <th>NAME</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                      <?php
            while($row = $q ->fetch(PDO::FETCH_OBJ)){
                            //echo '<br>';
                            echo '<tr>';
                            echo '<td>'. $row->ID."\n".'</td>';
                            echo '<td>'. $row->eng."\n".'</td>';
                            echo '<td>'. $row->chi."\n".'</td>';
                            echo '<td>'. $row->name."\n".'</td>';
                            echo '<td width=250>';
                            echo '<a class="btn btn-primary" href="READ.php?ID='.$row->ID.'">Read</a>';
                            echo ' ';
                            echo '<a class="btn btn-success" href="update.php?id='.$row->ID.'">Update</a>';
                            //echo ' ';
                            //echo '<a class="btn btn-danger" href="delete.php?id='.$row->ID.'">Delete</a>';
                            echo '</td>';
                            echo '</tr>';
                   }
            
            Database::disconnect();
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<meta http-equiv="Content-Type" content="text/html" charset = "utf-8">
</head>
<body>
    <div class="container">
     
                <div class="span10 offset1">
                    <div class="row">
                        <h3>搜尋資料庫</h3>
                    </div>
             
                    <form class="form-horizontal" action="updatehone.php" method="post">
                      
                      <div class="control-group <?php echo !empty($engError)?'error':'';?>">
                        <label class="control-label">輸入ID</label>
                        <div class="controls">
                            <input name="id" type="text" placeholder="輸入ID 可不填" style="width:200px;height:20px;" >
                        </div>
                      </div>
                      <div class="control-group">
                        <label class="control-label">英文原文</label>
                        <div class="controls">
                            <input name="eng" type="text" placeholder="輸入英文" style="width:400px;height:120px;" >
                        </div>
                      </div>
                      <div class="control-group">
                        <label class="control-label">台灣中文翻譯</label>
                        <div class="controls">
                            <input name="chi" type="text" placeholder="輸入台灣中文" style="width:400px;height:120px;" >
                        </div>
                      </div>
                      <!--
                      <div class="control-group">
                        <label class="control-label">大陸中文翻譯</label>
                        <div class="controls">
                            <input name="chicn" type="text" placeholder="輸入大陸中文" style="width:400px;height:120px;" >
                        </div>
                      </div>
                      <div class="control-group">
                        <label class="control-label">香港中文翻譯</label>
                        <div class="controls">
                            <input name="chihk" type="text" placeholder="輸入香港中文" style="width:400px;height:120px;" >
                        </div>
                      </div>
                      <div class="control-group">
                        <label class="control-label">新加坡中文翻譯</label>
                        <div class="controls">
                            <input name="chisig" type="text" placeholder="輸入新加坡中文" style="width:400px;height:120px;" >
                        </div>
                      </div>
                      <div class="control-group">
                        <label class="control-label">馬來文翻譯</label>
                        <div class="controls">
                            <input name="mal" type="text" placeholder="輸入馬來文" style="width:400px;height:120px;" >
                        </div>
                      </div>
                      -->
                      <div class="form-actions">
                          <button type="submit" class="btn btn-success">查詢</button>
                          <a class="btn btn-danger" href="home.php">返回</a>
                        </div>
                    </form>
                </div>
    </div> <!-- /container -->
</tbody>
</table>
</body>
</html>
            

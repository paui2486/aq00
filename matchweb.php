<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<?php
 
//如要讀取 引入讀取檔案即可
require_once('/var/www/html/AQ/PHPExcel/Classes/PHPExcel.php');// 輸出用
require_once('/var/www/html/AQ/PHPExcel/Classes/PHPExcel/IOFactory.php');//讀取用
include 'PDOCON.php';

// 讀excel 設定 開始
$inputFileName = $newxlsx;// 需要被翻譯的 動態化
//echo $inputFileName."路徑";
$inputFileType = PHPExcel_IOFactory::identify($inputFileName);// 讓程式自動判別副檔名(excel版本)

$reader = PHPExcel_IOFactory::createReader($inputFileType); // 讀取2007 excel 檔案

$PHPExcel = $reader->load($inputFileName); // 檔案名稱 需已經上傳到主機上

$sheet = $PHPExcel->getSheet(1); // 讀取第一個工作表(編號從 0 開始) 注意他們都是一開始

$HighestColumn = $sheet->getHighestColumn(); //最大欄位的英文代號 第一行=A 第二行=B 這裡是G

$highestRow = $sheet->getHighestRow(); // 取得總列數 數字編號。A=0, B=1, C=2
// 讀excel 設定 結束

//$today = (date("Ymd"));
set_time_limit(0);// 這個CODE不受到運行時間限制
$objPHPExcel = new PHPExcel(); //實作一個 PHPExcel

$objPHPExcel->getProperties()->setCreator("PHP") //建立者
        ->setLastModifiedBy("PHP")//上次修改
        ->setTitle("Title標題") //標題
        ->setSubject("Subject副標題")//副標題
        ->setDescription("Description說明")//說明
        ->setKeywords("Keywords關鍵字")//關鍵字
        ->setCategory("Category分類");//分類

//設定操作中的工作表
$objPHPExcel->setActiveSheetIndex(0); //指定目前要編輯的工作表 ，預設0是指第一個工作表
$sheetX = $objPHPExcel->getActiveSheet();


//將工作表命名
$sheetX->setTitle('第一張表');//第一個工作表 名稱

$sheetX->getColumnDimension('A')->setWidth(50); //設定欄寬
$sheetX->getColumnDimension('B')->setWidth(50);
$sheetX->getColumnDimension('C')->setWidth(50);
$sheetX->getColumnDimension('D')->setWidth(50);
$sheetX->getColumnDimension('E')->setWidth(50);
//// 這以上是存檔需要的宣告 以下是讀檔必備的資料
//檔案路徑



for($row=0;$row<=$highestRow;$row++){
$sheetX->getStyle('A'.$row)->getAlignment()->setWrapText(true);//單格設定 自動換列
$sheetX->getStyle('B'.$row)->getAlignment()->setWrapText(true);
$sheetX->getStyle('C'.$row)->getAlignment()->setWrapText(true);
}

$sheetX->setCellValue('A1','         　　 Aquaview Co. Ltd.');
$sheetX->getStyle('A1')->getFont()->setBold(true);
$sheetX->getStyle('A1')->getFont()->setSize(16);
$sheetX->setCellValue('A2','　　         目川文化數位股份有限公司');
$sheetX->getStyle('A2')->getFont()->setBold(true);
$sheetX->getStyle('A2')->getFont()->setSize(11);
$sheetX->setCellValue('A3','　　          Solution to Creative Learning');
$sheetX->setCellValue('A5','URL');
$sheetX->setCellValue('A6','各語言字數');
$sheetX->setCellValue('A7','第幾篇');
$sheetX->setCellValue("B"."9","TC (繁體版)");
//$sheetX->setCellValue("D"."9","SC (簡體版)");
//$sheetX->setCellValue("E"."9","MA (馬來版)");

$objDrawing = new PHPExcel_Worksheet_Drawing();

$objDrawing->setName('目川文化');

$objDrawing->setDescription('目川文化');

$objDrawing->setPath('/var/www/html/AQ/culture.png');

$objDrawing->setHeight(66);

$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());

$TotalColumn = (ord($HighestColumn)-(64));// 將"任意"字符轉換為 ASCII 碼: 這裡是將G轉換71-64=7 
//$TotalColumn1 = (ord($HighestColumn1)-(64));
$Newval = array();
$valc = '';
//echo $highestRow;
//echo $TotalColumn;
// 一次讀取一列
for ($row = 11; $row <= $highestRow; $row++) {//直的ROW 可以從 11 開始
    for ($column = 0; $column < $TotalColumn; $column++) {//看你有幾個欄位 橫的 
        $val = trim($sheet->getCellByColumnAndRow($column, $row)->getValue());
        if(preg_match("/^\*URL../i", "$val")){//如果VAL開頭是URL就處理
                $Newval = preg_split("/[\s]+/",$val);
                $Newval[0] = null; //不應該 變成NULL 應該要把順序往前提
                $Newval[1] = null;
                array_splice($Newval,0,2);//
                $val = implode(" ",$Newval);
                $Newval = preg_split("/[\s]+/",$val);
                $Newval = preg_replace("/\d+/","%",$Newval);/**/
                $valc = implode(" ",$Newval); //相容的
                
                
        }else if(preg_match("/^\URL../i", $nstr[($i-1)])){
				$Newval = preg_split("/[\s]+/",trim($nstr[($i-1)]));
                $Newval[0] = null; //不應該 變成NULL 應該要把順序往前提
                $Newval[1] = null;
                array_splice($Newval,0,2);//
                $val = implode(" ",$Newval);
                $Newval = preg_split("/[\s]+/",$val);
                $Newval = preg_replace("/\d+/","%",$Newval);
                $valc = implode(" ",$Newval); //相容的
				$valc = trim("$valc");
        }else if(preg_match("/^Airfare is not included in the trip price\!.*We highly recommend that you purchase travel insurance for any trip.$/","$val")){
            //註解 頭尾比較關鍵字 .*可以 類是SQL%功能 .是任意字元 *: 出現 0 次或以上
            $val = "Airfare is not included in the trip price! The closest commercial airport to the % is % International Airport % We highly recommend that you purchase travel insurance for any trip.";
            $Newval = preg_split("/[\s]+/",$val);//根據空白切開 意思說 這邊根據單字做比對
            $Newval = preg_replace("/[\d]+/","%",$Newval);
            $valc = implode(" ",$Newval); //相容的
        }else if(preg_match("/^Airfare is not included in the trip price\!.*Please note: Airport transfers are not included for.*.$/","$val")){
            //註解 頭尾比較關鍵字 .*可以 類是SQL%功能 .是任意字元 *: 出現 0 次或以上
            $val = "Airfare is not included in the trip price! The closest commercial airport to the hotel is %. Please note: Airport transfers are not included for %";
            $Newval = preg_split("/[\s]+/",$val);//根據空白切開 意思說 這邊根據單字做比對
            $Newval = preg_replace("/[\d]+/","%",$Newval);
            $valc = implode(" ",$Newval); //相容的
			$valc = trim("$valc");
        }else if(preg_match("/^DAY.[0-9].*$/i","$val")){
            //註解 頭尾比較關鍵字 .*可以 類是SQL%功能 .是任意字元 *: 出現 0 次或以上
            $Newval = preg_split("/[\s]+/",("$val"));//根據空白切開 意思說 這邊根據單字做比對
            $valc = implode(" ",$Newval);
            $valc = trim("$valc");
            
        
        }else if(preg_match("/^. p\.m\..*$|. a\.m\..*$/i","$val")){
            //註解 頭尾比較關鍵字 .*可以 類是SQL%功能 .是任意字元 *: 出現 0 次或以上
            $Newval = preg_split("/[\s]+/",("$val"));//根據空白切開 意思說 這邊根據單字做比對
            $valc = implode(" ",$Newval);
            $valc = trim("$valc");
            //TC無此段
        }else if(preg_match("/^TC無此段.*./i","$val")){
            //註解 頭尾比較關鍵字 .*可以 類是SQL%功能 .是任意字元 *: 出現 0 次或以上
            $Newval = preg_split("/[\s]+/",("$val"));//根據空白切開 意思說 這邊根據單字做比對
            $valc = "— — —";
            
        }else if(preg_match("/^Your DreamTrip includes.*.days and.*.nights$/i","$val")){
            //註解 頭尾比較關鍵字 .*可以 類是SQL%功能 .是任意字元 *: 出現 0 次或以上
            $Newval = preg_split("/[\s]+/",("$val"));//根據空白切開 意思說 這邊根據單字做比對
            $valc = implode(" ",$Newval);
            $valc = trim("$valc");
            
        }else if(preg_match("/^. p\.m\..*$|. a\.m\..*$/i","$val")){
            
            $Newval = preg_split("/[\s]+/",($nstr[($i-1)]));
            $valc = implode(" ",$Newval);
            $valc = trim("$valc");
            
        }else if(preg_match("/^•	/i","$val")){//
            //註解 頭尾比較關鍵字 .*可以 類是SQL%功能 .是任意字元 *: 出現 0 次或以上
            $Newval = preg_split("/[\s]+/",("$val"));
			array_splice($Newval,0,1);//
			$valc = "%".(implode(" ",$Newval));
            $valc = trim("$valc");
            
        }else{ 
            $Newval = preg_split("/[\s]+/",$val);//根據空白切開 意思說 這邊根據單字做比對
            $Newval = preg_replace("/[\d]+/","%",$Newval);
            $valc = implode(" ",$Newval); //相容的
			$valc = trim("$valc");
             }
        // 開始比對
            /*
            print_r($Newval);
            echo ("<BR>");
            echo $valc;
            exit;
            */
        
        $pdo = Database::connect();
        $sql = 'SELECT eng,chi FROM translation where eng LIKE ? ORDER BY timeatamp DESC LIMIT 1'; //嘗試用LIKE如果直接比對 eng 的話 替換要比對的資料陣列為%
        $result=$pdo->prepare($sql);
        $result->execute(array($valc));// 將相容的資料塞入 LIKE 去搜尋
        Database::disconnect();
        $rw = $result->fetch(PDO::FETCH_OBJ);
        if($rw != null){
            
               $translation = $rw->chi;    //跑到第幾個就叫出修對應的中文
               $chicn = $rw->chicn;
               $Melayu = $rw->mal;
               $original = $sheet->getCellByColumnAndRow((($column)), $row)->getValue();
               //$original = $val;
               $sheetX->setCellValue("A".($row),$original);
               
               $sheetX->setCellValue("B".($row),$translation);
               
               //$sheetX->setCellValue("C".($row),$chicn);// 簡體
               
               //$sheetX->setCellValue("D".($row),$Melayu);// 馬來
               /*
               echo ($original);
               echo("<BR>");
               echo ($translation);
               echo("<BR>");
               echo ('有進來');
               echo("<BR>");
               exit ;
               */
        }else{
            $original = $val;
            //$original = $sheet->getCellByColumnAndRow((($column)), $row)->getValue();
            $sheetX->setCellValue("A".($row),$original);
            /*
            echo ('沒有進來');
            echo("<BR>");
            echo ($original);
            exit;
            */
        }

    }//橫

}//直

// 存檔必須宣告的必要資訊

$filename = ($startX);
ob_end_clean();

$filename = $filename.'以比對'.'.xlsx';

header("Content-type: text/html; charset=utf-8");
header("Content-Type: application/vnd.ms-excel");
header("Content-Disposition: attachment;filename=".$filename);
header("Cache-Control: max-age=0");

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
//$objWriter->save('filenameX.xlsx');// 另存成指定檔名

exit;// 注意 這個exit是必要的 雖然沒有程式 也會正常執行刑但是最後產生的excel會有問題

?>
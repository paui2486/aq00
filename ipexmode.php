<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<?php
 
//如要讀取 引入讀取檔案即可
require_once ('/var/www/html/AQ/PHPExcel/Classes/PHPExcel.php');
require_once ('/var/www/html/AQ/PHPExcel/Classes/PHPExcel/IOFactory.php');
require 'PDOCON.php';


//$inputFileName = "TM50.xlsx";
$inputFileName = $newxlsx;// 被上傳的內容 

$inputFileType = PHPExcel_IOFactory::identify($inputFileName);// 讓程式自動判別副檔名(excel版本)

$reader = PHPExcel_IOFactory::createReader($inputFileType); // 讀取2007 excel 檔案

$PHPExcel = $reader->load($inputFileName,$encode='utf-8'); // 檔案名稱 需已經上傳到主機上 後面是 編碼格式

$sheet = $PHPExcel->getSheet(1); // 讀取第一個工作表(編號從 0 開始) OOXX的T30 第2工作表

$HighestColumn = $sheet->getHighestColumn(); //最大欄位的英文代號 第一行=A 第二行=B 這裡是G

$highestRow = $sheet->getHighestRow(); // 取得總列數 數字編號。A=0, B=1, C=2

$TotalColumn = (ord($HighestColumn)-(64));// 將"任意"字符轉換為 ASCII 碼: 這裡是將G轉換71-64=7

echo '總共 '.$TotalColumn.' 行<br>';
echo '總共 '.$highestRow.' 列<br>';



function TC($TotalColumn,$TC,$VAL,$sheet){
    for($i=0;$i<=$TotalColumn;$i++){// 讓他下去沿著地9行下去跑並找到TC位置儲存
     for($h=6;$h<=10;$h++){ //抓語言的範圍
    $VAL = $sheet->getCellByColumnAndRow("$i", "$h")->getValue();
    if (preg_match("/TC..繁體版./", "$VAL")) {//TC （繁體版）
     $TC =$i;
     return $TC;
    }else if(preg_match("/TC..繁体版./", "$VAL")){
     $TC =$i;
     return $TC;
    }else if(preg_match("/TC.繁體版./", "$VAL")){
     $TC =$i;
     return $TC;
    }else {
        continue;
    }
    }
    }
}
function SC($TotalColumn,$SC,$VAL,$sheet){//Malay
    for($i=0;$i<=$TotalColumn;$i++){// 讓他下去沿著地9行下去跑並找到TC位置儲存
     for($h=6;$h<=10;$h++){
    $VAL = $sheet->getCellByColumnAndRow("$i", "$h")->getValue();
    if (preg_match("/SC..簡體版./", "$VAL")) {
     $SC =$i;
     return $SC;
    } elseif (preg_match("/SC..简体版./", "$VAL")) {
     $SC =$i;
     return $SC;
    } elseif (preg_match("/SC (簡體版)/", "$VAL")) {
     $SC =$i;
     return $SC;
    } else {
        continue;
    }//SC (简体版)
    }
    }
}
function MALAY($TotalColumn,$MA,$VAL,$sheet){//Malay
    for($i=0;$i<=$TotalColumn;$i++){// 讓他下去沿著地9行下去跑並找到TC位置儲存
     for($h=6;$h<=10;$h++){
    $VAL = $sheet->getCellByColumnAndRow("$i", "$h")->getValue();
    if (preg_match("/Malay/", "$VAL")) {
     $MA =$i;
     return $MA;
    } elseif (preg_match("/MA..馬來文./", "$VAL")) {
     $MA =$i;
     return $MA;
    } elseif (preg_match("/MA.馬來文./", "$VAL")) {
     $MA =$i;
     return $MA;
    } else {
        continue;
    }
    }
    }
}
$DAY = 0;// 掠過天數用

$TC = TC($TotalColumn,$TC,$VAL,$sheet);
$SC = SC($TotalColumn,$SC,$VAL,$sheet);
$MALAY = MALAY($TotalColumn,$MA,$VAL,$sheet);

/*
for($i=0;$i<=$TotalColumn;$i++){// 讓他下去沿著地9行下去跑並找到TC位置儲存
    $VAL = $sheet->getCellByColumnAndRow("$i", "9")->getValue();
    if (preg_match("/TC..繁體版./", "$VAL")) {
     $TC =$i;
    } else {
        continue;
    }
}
for($i=0;$i<=$TotalColumn;$i++){// 讓他下去沿著地9行下去跑並找到SC位置儲存
    $VAL = $sheet->getCellByColumnAndRow("$i", "9")->getValue();
    if (preg_match("/SC..簡體版./", "$VAL")) {
     $SC =$i;
    } else {
        continue;
    }
}
*/
//
echo '<BR>';
echo ("繁中"."$TC");
echo '<BR>';
echo ("簡中"."$SC");
echo '<BR>';
echo ("馬來"."$MALAY");
echo '<BR>';
echo '<BR>';
//
$A=array();//英文
$B=array();//繁中
$C=array();//簡中
$F=array();//馬來
/*
$E=array();
$F=array();
$G=array();
$H=array();
$I=array();
*/
$VAL=array();
// A0 B1 C2 D3 E4 F5 G6 H7 I8 J9 K10
for($row=11;$row<=$highestRow;$row++){// row 可以從11開始根據原始資料

$original = $sheet->getCellByColumnAndRow("0", $row)->getValue();// 再次備註 讀取要用數字 0A 英文
if(is_object($original))  $original= $original->__toString();

$translation = $sheet->getCellByColumnAndRow("$TC", $row)->getValue();//B1 繁中
if(is_object($translation))  $translation= $translation->__toString();

if($SC != null){
$cn = $sheet->getCellByColumnAndRow("$SC", $row)->getValue();//C2 簡中
if(is_object($cn))  $cn= $cn->__toString();
}else{}
//這邊追加判斷式 如果 TCSCMA 等行變數 = 0 或是 null 即為不正常
if($MALAY != null){
$MA = $sheet->getCellByColumnAndRow("$MALAY", $row)->getValue();//D3 馬來
if(is_object($MA))  $MA= $MA->__toString();
}else{}
/*
if ($MA != 0 || !null){
    $hk = $sheet->getCellByColumnAndRow("3", $row)->getValue();//D3 港中
    if(is_object($hk))  $hk= $hk->__toString();
}else{continue;}
if ($MA != 0 || !null){
    $hk = $sheet->getCellByColumnAndRow("3", $row)->getValue();//D3 新中
    if(is_object($hk))  $hk= $hk->__toString();
}else{continue;}
if ($MA != 0 || !null){
    $hk = $sheet->getCellByColumnAndRow("3", $row)->getValue();//D3 韓文
    if(is_object($hk))  $hk= $hk->__toString();
}else{continue;}
if ($MA != 0 || !null){
    $hk = $sheet->getCellByColumnAndRow("3", $row)->getValue();//D3 日文
    if(is_object($hk))  $hk= $hk->__toString();
}else{continue;}
if ($MA != 0 || !null){
    $hk = $sheet->getCellByColumnAndRow("3", $row)->getValue();//D3 泰文
    if(is_object($hk))  $hk= $hk->__toString();
}else{continue;}    
*/

if (preg_match("/^.DAY|^Day..|^Day...|^..Day..|^Day/i", "$original")|| preg_match("/Check out this DreamTrip on Facebook./", "$original") || preg_match("/^Airport.Transfers/", "$original") || preg_match("/^\*URL..$/", "$original")|| preg_match("/^DEPART/i", "$original")|| preg_match("/^ARRIVE/i", "$original")|| preg_match("/^PORT/i", "$original")) { //PORT || preg_match("/^..$/i", "$original")
     $DAY = 1;
} else {
     $DAY = 0; //這個0是必要的 否則第2圈開始 DAY會一直是1
}
// *紅字發翻譯 *URL 2 等
if (preg_match("/^\*紅字發翻譯/i", "$original")){
    $VAL = preg_split("/[\s]+/",$original);
    $VAL[0] = null;
    $original = trim(implode(" ",$VAL));
    
}else if(preg_match("/^\*URL../i", "$original")){
    $VAL = preg_split("/[\s]+/",$original);
    $VAL[0] = null;
    $VAL[1] = null;
    $original = trim(implode(" ",$VAL));//合併成字串
    
}else if(preg_match("/^\*URL./i", "$original")){
    $VAL = preg_split("/[\s]+/",$original);
    $VAL[0] = null;
    $original = trim(implode(" ",$VAL));
    
}else if(preg_match("/^URL../i", "$original")){
    $VAL = preg_split("/[\s]+/",$original);
    $VAL[0] = null;
    $VAL[1] = null;
    $original = trim(implode(" ",$VAL));
    
}else if(preg_match("/^URL./i", "$original")){
    $VAL = preg_split("/[\s]+/",$original);
    $VAL[0] = null;
    $original = trim(implode(" ",$VAL));
    
}else if(preg_match("/^此篇無標題/i", "$original")){
    $VAL = preg_split("/[\s]+/",$original);
    $VAL[0] = null;
    $original = trim(implode(" ",$VAL));// trim 去除空白
}


//— — —
if ($original ==null || $DAY ==1 || $original == ' — — — ' || $original == '— — — ' || $original == '— — —'|| $original == '---'|| $original == 'What we love:' || $original == 'Transportation' || $original == 'Hotel features:' || $original == '• Telephone.'|| $original == '• Television.'|| $original == '•Television.'|| $original == '• Individual climate control.'|| $original == '• Private bathroom and shower.'|| $original == 'Itinerary subject to change.'|| $original == '•Duty-free shopping.'|| $original == '•Access to a casino.'|| $original == 'Round-trip airport transfers are included from:'|| $original == 'At the time of booking, you will see a page with your order number and trip cost. This is your confirmation. Please retain and bring this information with you on your trip.'|| $original =='• Round-trip airport transfers.'|| $original =='•Mandarin- or English-speaking guide.'|| $original =='Life is great when you’re a Platinum Member!' || $original == 'URL 1'){//
    continue;
}else{
$A["$row"] = trim($original);// 英文
$B["$row"] = trim($translation);// 繁中文
$C["$row"] = trim($cn);// 簡中文
$F["$row"] = trim($MA);// 馬來文
/*
$D["$row"] = $hk;// 香港中文
$E["$row"] = $sig;// 新加坡中文
$G["$row"] = $kor;// 韓文
$H["$row"] = $jpn;// 日文
$I["$row"] = $tha;// 泰文
*/
}
/*

*/
/* 將數值存入 AB陣列 A陣列處理重複值銷除 B要如何同步消除  SQL存入*/
}
$A = array_reverse($A);//以相反的元素顺序返回数组：反轉順序
$A = array_unique($A); //只需獨特英文 將重複的數值去除 因為這邊是確定的DB 不用擔心 資料的原始性
$B = array_reverse($B);
$C = array_reverse($C);
$F = array_reverse($F);
/*
$D = array_reverse($D); //港
$E = array_reverse($E); //新
$G = array_reverse($G); //韓
$H = array_reverse($H); //日
$I = array_reverse($I); //泰
*/

print_r($A);
echo "<BR>";
echo "<BR>";
print_r($B);
echo "<BR>";
echo "<BR>";
if($C["0"] != null){
print_r($C);
echo "<BR>";
echo "<BR>";
}else{}
if($F["0"] != null){//這邊追加判斷式 如果$F["0"]為空 則不要顯示 注意 應為反轉了 不再是11開使而是0 其他以此類推
print_r($F);
echo "<BR>";
echo "<BR>";
}else{}
/**/
echo "&nbsp;&nbsp;".count($A)."<BR>"; //檢測陣列長度


for($row=0;$row<=$highestRow;$row++){ //未反轉處理用11開始 以反轉 0開始
    if($A["$row"] == null){continue;}else{
        //先select 不為空的資料 DB 是否有了(先做只筆對英文,同時比對英文中文感覺比較保險)如果有了 跳過 沒有一樣存
        $pdo = Database::connect();
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = "SELECT eng,chi,chicn,chisig,chihk,mal FROM translation where eng = ?";
        $q = $pdo->prepare($sql);
        $q->execute(array($A["$row"]));
        $data = $q->fetch(PDO::FETCH_ASSOC);
        Database::disconnect();
        if ($data != null){//continue;
        // 如果英文原本有了就看有沒有馬來 有就跳過沒有更新
        if($data->mal == null){
            
        $pdo = Database::connect();
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = "UPDATE `translation` SET mal = ? where eng = ?";
        $p = $pdo->prepare($sql);
        $p->execute(array(($F["$row"]),($A["$row"])));
        Database::disconnect();
            
        }else{}
        
        }elseif ($data == null){
            //echo "$row <BR>";
            $pdo = Database::connect();
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "INSERT INTO translation (name,eng,chi,chicn,chihk,chisig,mal) values(?, ?, ?, ?, ?, ?, ?)";//
            $q = $pdo->prepare($sql);
            $q->execute(array($name,$A[$row],$B[$row],$C[$row],$chihk,$chisig,$F[$row]));
            Database::disconnect();
              
            //header("Location: home.php");    
            //echo("<meta http-equiv=REFRESH CONTENT=6;url=hone.php>");//6秒後跳轉 正式上線後啟用
        }

    }
}



?>
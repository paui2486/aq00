<?php
    session_start();
    if($_SESSION['name'] != null)
    {

    }
    else
    {
        echo("權限不足，動導向致使首頁");
        echo("<meta http-equiv=REFRESH CONTENT=2;url=login.php>");
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<meta http-equiv="Content-Type" content="text/html" charset = "utf-8">
</head>
 
<body>
    <div class="container">
     
                <div class="span10 offset1">
                    <div class="row">
                        <h3>輸入英文整理</h3>
                    </div>
             
                    <form class="form-horizontal" action="Splits.php" method="post">
					
                      <div class="control-group">
                        <label class="control-label">檔案名稱</label>
                        <div class="controls">
                            <input name="filename" type="text" placeholder="輸入檔名" style="width:200px;height:20px;">
                            
                      </div>
                      <div class="control-group">
                        <label class="control-label">英文文章</label>
                        <div class="controls">
                            <input name="eng" type="text" placeholder="輸入英文" style="width:400px;height:120px;" value="<?php echo !empty($eng)?$eng:'';?>">
                            
                        </div>
                      </div>
                      <div class="form-actions">
                          <button type="submit" class="btn btn-success">送出</button>
                          <a class="btn btn-danger" href="home.php">返回</a>
                        </div>
                    </form>
                </div>
                 
    </div> <!-- /container -->
  </body>
</html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<?php
require_once ('/var/www/html/AQ/PHPExcel/Classes/PHPExcel.php');
require_once ('/var/www/html/AQ/PHPExcel/Classes/PHPExcel/IOFactory.php');

/*$str = 'Spend an action-packed day at one of the world’s top amusement/theme parks on this DreamTrip to Osaka.
AssignToggle Dropdown
<span style="font-family: Arial;">This 1-day all-access pass to Universal Studios Japan® provides a variety of world-class entertainment for all ages. Roam the halls of the towering Hogwarts™ castle — or fly above it during a game of quidditch — at The Wizarding World of Harry Potter™, a themed attraction based on the “Harry Potter” book and film series. Then, escape from dinosaurs and man-eating sharks on rides based on the major motion pictures “Jurassic Park” and “Jaws.” Finally, shop on Hello Kitty Fashion Avenue, or play with your favorite “Peanuts” characters at Snoopy Studio™. <br><br><strong>What we love: </strong><br>• Universal Studios Japan® is ranked fifth among the top amusement/theme parks in the world. <br>• All-access pass to Universal Studios Japan®; lunch and dinner meal coupons and transportation to and from the park are included.<br>• Namba Station, one of Osaka’s major railway stations, is within walking distance of the Hotel Monterey Grasmere Osaka. <br><br><div>Check out this DreamTrip on <strong><a href="https://www.facebook.com/events/1712762308975070/" target="_blank">Facebook</a></strong>.</div><br><br><strong>This trip will close by June 10. </strong></span><br>
AssignToggle Dropdown';*/
$str = $_POST['eng'];
$FN = $_POST['filename'];




$str = str_replace("nbsp","+","$str");
$str = str_replace("⏎"," ","$str");
$str = str_replace("+","<br>","$str");
$str = str_replace("<br><br>","<br>","$str");
$cstr = strip_tags($str, "<br>");//整理過後的字串 過濾 程式標籤符號

//echo $cstr;

//$nstr = preg_split("/\./", $cstr);//根據 . 分割
$nstr = preg_split("/<br>|AssignToggle Dropdown/", $cstr);
$NC = count($nstr); //小於等於使用
//print_r($nstr);
//exit();
$objPHPExcel = new PHPExcel(); //實作一個 PHPExcel

//print_r($nstr);
//echo $FN;

//設定操作中的工作表
$objPHPExcel->setActiveSheetIndex(0); //指定目前要編輯的工作表 ，預設0是指第一個工作表
$sheet = $objPHPExcel->getActiveSheet();
 
//將工作表命名
$sheet->setTitle('第一張表');//第一個工作表 名稱

$sheet->getColumnDimension('A')->setWidth(50); //設定欄寬
$sheet->getColumnDimension('B')->setWidth(50);
$sheet->getColumnDimension('C')->setWidth(50);
$sheet->getColumnDimension('D')->setWidth(50);
$sheet->getColumnDimension('E')->setWidth(50);
//print_r ($nstr) ;

$sheet->setCellValue('A1','         　　 Aquaview Co. Ltd.');
$sheet->getStyle('A1')->getFont()->setBold(true);
$sheet->getStyle('A1')->getFont()->setSize(16);
$sheet->setCellValue('A2','　　         目川文化數位股份有限公司');
$sheet->getStyle('A2')->getFont()->setBold(true);
$sheet->getStyle('A2')->getFont()->setSize(11);
$sheet->setCellValue('A3','　　          Solution to Creative Learning');
$sheet->setCellValue('A5','URL');
$sheet->setCellValue('A6','各語言字數');
$sheet->setCellValue('A7','第幾篇');
$sheet->setCellValue("C"."9","TC (繁體版)");
$sheet->setCellValue("D"."9","SC (簡體版)");
$sheet->setCellValue("E"."9","MA (馬來版)");
$sheet->setCellValue('A10','文章名稱');
//接下來要寫一個 迴圈自動換列
for($i=1;$i<=$NC;$i++){
    
    $sheet->getStyle("A".($i+10))->getAlignment()->setWrapText(true);
    
}

//儲存格內容
for($i=1;$i<=$NC;$i++){
    
    $sheet->setCellValue("A".($i+10),trim($nstr[($i-1)]));
    
}

ob_end_clean();
$objPHPExcel = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

$filename = $FN.'.xlsx';
header("Content-type: text/html; charset=utf-8");
header("Content-Type: application/vnd.ms-excel");
header("Content-Disposition: attachment;filename=".$filename);
header("Cache-Control: max-age=0");


$objPHPExcel->save('php://output');
//$objWriter->save('filename.xlsx');// 另存成指定檔名
//exit;

?>